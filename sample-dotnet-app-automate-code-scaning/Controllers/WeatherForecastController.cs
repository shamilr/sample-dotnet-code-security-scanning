﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace sample_dotnet_app_automate_code_scaning.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public bool SubmitData(string query)
        {
            using (SqlConnection conn = new SqlConnection("server=.;database=db;user=sa;password=123; Connection Timeout=400;  Command Timeout=400; TrustServerCertificate=true"))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("DELETE FROM GasReading WHERE GasReadingID = " + query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
            return true;
        }

        [HttpPost]
        public bool SubmitData2(string query)
        {
            using (var connection = new SqlConnection("server=.;database=db;user=sa;password=123; Connection Timeout=400;  Command Timeout=400; TrustServerCertificate=true"))
            {
                connection.Open();
                connection.Execute("DELETE FROM GasReading WHERE GasReadingID = " + query);
            }
            return true;
        }

        [HttpGet]
        public string GetFile(string filename)
        {
            return System.IO.File.ReadAllText(filename);
        }
    }
}
